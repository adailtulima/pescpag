package br.ufpa.castanhal.web.rest;

import static br.ufpa.castanhal.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import br.ufpa.castanhal.IntegrationTest;
import br.ufpa.castanhal.domain.Ingresso;
import br.ufpa.castanhal.repository.IngressoRepository;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link IngressoResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class IngressoResourceIT {

    private static final LocalDate DEFAULT_DIA = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DIA = LocalDate.now(ZoneId.systemDefault());

    private static final Double DEFAULT_VALOR = 1D;
    private static final Double UPDATED_VALOR = 2D;

    private static final ZonedDateTime DEFAULT_HORA_SAIDA = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_HORA_SAIDA = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_HORA_ENTRADA = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_HORA_ENTRADA = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String ENTITY_API_URL = "/api/ingressos";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private IngressoRepository ingressoRepository;

    @Mock
    private IngressoRepository ingressoRepositoryMock;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restIngressoMockMvc;

    private Ingresso ingresso;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Ingresso createEntity(EntityManager em) {
        Ingresso ingresso = new Ingresso()
            .dia(DEFAULT_DIA)
            .valor(DEFAULT_VALOR)
            .horaSaida(DEFAULT_HORA_SAIDA)
            .horaEntrada(DEFAULT_HORA_ENTRADA);
        return ingresso;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Ingresso createUpdatedEntity(EntityManager em) {
        Ingresso ingresso = new Ingresso()
            .dia(UPDATED_DIA)
            .valor(UPDATED_VALOR)
            .horaSaida(UPDATED_HORA_SAIDA)
            .horaEntrada(UPDATED_HORA_ENTRADA);
        return ingresso;
    }

    @BeforeEach
    public void initTest() {
        ingresso = createEntity(em);
    }

    @Test
    @Transactional
    void createIngresso() throws Exception {
        int databaseSizeBeforeCreate = ingressoRepository.findAll().size();
        // Create the Ingresso
        restIngressoMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(ingresso)))
            .andExpect(status().isCreated());

        // Validate the Ingresso in the database
        List<Ingresso> ingressoList = ingressoRepository.findAll();
        assertThat(ingressoList).hasSize(databaseSizeBeforeCreate + 1);
        Ingresso testIngresso = ingressoList.get(ingressoList.size() - 1);
        assertThat(testIngresso.getDia()).isEqualTo(DEFAULT_DIA);
        assertThat(testIngresso.getValor()).isEqualTo(DEFAULT_VALOR);
        assertThat(testIngresso.getHoraSaida()).isEqualTo(DEFAULT_HORA_SAIDA);
        assertThat(testIngresso.getHoraEntrada()).isEqualTo(DEFAULT_HORA_ENTRADA);
    }

    @Test
    @Transactional
    void createIngressoWithExistingId() throws Exception {
        // Create the Ingresso with an existing ID
        ingresso.setId(1L);

        int databaseSizeBeforeCreate = ingressoRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restIngressoMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(ingresso)))
            .andExpect(status().isBadRequest());

        // Validate the Ingresso in the database
        List<Ingresso> ingressoList = ingressoRepository.findAll();
        assertThat(ingressoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllIngressos() throws Exception {
        // Initialize the database
        ingressoRepository.saveAndFlush(ingresso);

        // Get all the ingressoList
        restIngressoMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(ingresso.getId().intValue())))
            .andExpect(jsonPath("$.[*].dia").value(hasItem(DEFAULT_DIA.toString())))
            .andExpect(jsonPath("$.[*].valor").value(hasItem(DEFAULT_VALOR.doubleValue())))
            .andExpect(jsonPath("$.[*].horaSaida").value(hasItem(sameInstant(DEFAULT_HORA_SAIDA))))
            .andExpect(jsonPath("$.[*].horaEntrada").value(hasItem(sameInstant(DEFAULT_HORA_ENTRADA))));
    }

    @SuppressWarnings({ "unchecked" })
    void getAllIngressosWithEagerRelationshipsIsEnabled() throws Exception {
        when(ingressoRepositoryMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restIngressoMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(ingressoRepositoryMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({ "unchecked" })
    void getAllIngressosWithEagerRelationshipsIsNotEnabled() throws Exception {
        when(ingressoRepositoryMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restIngressoMockMvc.perform(get(ENTITY_API_URL + "?eagerload=false")).andExpect(status().isOk());
        verify(ingressoRepositoryMock, times(1)).findAll(any(Pageable.class));
    }

    @Test
    @Transactional
    void getIngresso() throws Exception {
        // Initialize the database
        ingressoRepository.saveAndFlush(ingresso);

        // Get the ingresso
        restIngressoMockMvc
            .perform(get(ENTITY_API_URL_ID, ingresso.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(ingresso.getId().intValue()))
            .andExpect(jsonPath("$.dia").value(DEFAULT_DIA.toString()))
            .andExpect(jsonPath("$.valor").value(DEFAULT_VALOR.doubleValue()))
            .andExpect(jsonPath("$.horaSaida").value(sameInstant(DEFAULT_HORA_SAIDA)))
            .andExpect(jsonPath("$.horaEntrada").value(sameInstant(DEFAULT_HORA_ENTRADA)));
    }

    @Test
    @Transactional
    void getNonExistingIngresso() throws Exception {
        // Get the ingresso
        restIngressoMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingIngresso() throws Exception {
        // Initialize the database
        ingressoRepository.saveAndFlush(ingresso);

        int databaseSizeBeforeUpdate = ingressoRepository.findAll().size();

        // Update the ingresso
        Ingresso updatedIngresso = ingressoRepository.findById(ingresso.getId()).get();
        // Disconnect from session so that the updates on updatedIngresso are not directly saved in db
        em.detach(updatedIngresso);
        updatedIngresso.dia(UPDATED_DIA).valor(UPDATED_VALOR).horaSaida(UPDATED_HORA_SAIDA).horaEntrada(UPDATED_HORA_ENTRADA);

        restIngressoMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedIngresso.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedIngresso))
            )
            .andExpect(status().isOk());

        // Validate the Ingresso in the database
        List<Ingresso> ingressoList = ingressoRepository.findAll();
        assertThat(ingressoList).hasSize(databaseSizeBeforeUpdate);
        Ingresso testIngresso = ingressoList.get(ingressoList.size() - 1);
        assertThat(testIngresso.getDia()).isEqualTo(UPDATED_DIA);
        assertThat(testIngresso.getValor()).isEqualTo(UPDATED_VALOR);
        assertThat(testIngresso.getHoraSaida()).isEqualTo(UPDATED_HORA_SAIDA);
        assertThat(testIngresso.getHoraEntrada()).isEqualTo(UPDATED_HORA_ENTRADA);
    }

    @Test
    @Transactional
    void putNonExistingIngresso() throws Exception {
        int databaseSizeBeforeUpdate = ingressoRepository.findAll().size();
        ingresso.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restIngressoMockMvc
            .perform(
                put(ENTITY_API_URL_ID, ingresso.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(ingresso))
            )
            .andExpect(status().isBadRequest());

        // Validate the Ingresso in the database
        List<Ingresso> ingressoList = ingressoRepository.findAll();
        assertThat(ingressoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchIngresso() throws Exception {
        int databaseSizeBeforeUpdate = ingressoRepository.findAll().size();
        ingresso.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restIngressoMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(ingresso))
            )
            .andExpect(status().isBadRequest());

        // Validate the Ingresso in the database
        List<Ingresso> ingressoList = ingressoRepository.findAll();
        assertThat(ingressoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamIngresso() throws Exception {
        int databaseSizeBeforeUpdate = ingressoRepository.findAll().size();
        ingresso.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restIngressoMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(ingresso)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Ingresso in the database
        List<Ingresso> ingressoList = ingressoRepository.findAll();
        assertThat(ingressoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateIngressoWithPatch() throws Exception {
        // Initialize the database
        ingressoRepository.saveAndFlush(ingresso);

        int databaseSizeBeforeUpdate = ingressoRepository.findAll().size();

        // Update the ingresso using partial update
        Ingresso partialUpdatedIngresso = new Ingresso();
        partialUpdatedIngresso.setId(ingresso.getId());

        partialUpdatedIngresso.horaEntrada(UPDATED_HORA_ENTRADA);

        restIngressoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedIngresso.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedIngresso))
            )
            .andExpect(status().isOk());

        // Validate the Ingresso in the database
        List<Ingresso> ingressoList = ingressoRepository.findAll();
        assertThat(ingressoList).hasSize(databaseSizeBeforeUpdate);
        Ingresso testIngresso = ingressoList.get(ingressoList.size() - 1);
        assertThat(testIngresso.getDia()).isEqualTo(DEFAULT_DIA);
        assertThat(testIngresso.getValor()).isEqualTo(DEFAULT_VALOR);
        assertThat(testIngresso.getHoraSaida()).isEqualTo(DEFAULT_HORA_SAIDA);
        assertThat(testIngresso.getHoraEntrada()).isEqualTo(UPDATED_HORA_ENTRADA);
    }

    @Test
    @Transactional
    void fullUpdateIngressoWithPatch() throws Exception {
        // Initialize the database
        ingressoRepository.saveAndFlush(ingresso);

        int databaseSizeBeforeUpdate = ingressoRepository.findAll().size();

        // Update the ingresso using partial update
        Ingresso partialUpdatedIngresso = new Ingresso();
        partialUpdatedIngresso.setId(ingresso.getId());

        partialUpdatedIngresso.dia(UPDATED_DIA).valor(UPDATED_VALOR).horaSaida(UPDATED_HORA_SAIDA).horaEntrada(UPDATED_HORA_ENTRADA);

        restIngressoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedIngresso.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedIngresso))
            )
            .andExpect(status().isOk());

        // Validate the Ingresso in the database
        List<Ingresso> ingressoList = ingressoRepository.findAll();
        assertThat(ingressoList).hasSize(databaseSizeBeforeUpdate);
        Ingresso testIngresso = ingressoList.get(ingressoList.size() - 1);
        assertThat(testIngresso.getDia()).isEqualTo(UPDATED_DIA);
        assertThat(testIngresso.getValor()).isEqualTo(UPDATED_VALOR);
        assertThat(testIngresso.getHoraSaida()).isEqualTo(UPDATED_HORA_SAIDA);
        assertThat(testIngresso.getHoraEntrada()).isEqualTo(UPDATED_HORA_ENTRADA);
    }

    @Test
    @Transactional
    void patchNonExistingIngresso() throws Exception {
        int databaseSizeBeforeUpdate = ingressoRepository.findAll().size();
        ingresso.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restIngressoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, ingresso.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(ingresso))
            )
            .andExpect(status().isBadRequest());

        // Validate the Ingresso in the database
        List<Ingresso> ingressoList = ingressoRepository.findAll();
        assertThat(ingressoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchIngresso() throws Exception {
        int databaseSizeBeforeUpdate = ingressoRepository.findAll().size();
        ingresso.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restIngressoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(ingresso))
            )
            .andExpect(status().isBadRequest());

        // Validate the Ingresso in the database
        List<Ingresso> ingressoList = ingressoRepository.findAll();
        assertThat(ingressoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamIngresso() throws Exception {
        int databaseSizeBeforeUpdate = ingressoRepository.findAll().size();
        ingresso.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restIngressoMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(ingresso)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Ingresso in the database
        List<Ingresso> ingressoList = ingressoRepository.findAll();
        assertThat(ingressoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteIngresso() throws Exception {
        // Initialize the database
        ingressoRepository.saveAndFlush(ingresso);

        int databaseSizeBeforeDelete = ingressoRepository.findAll().size();

        // Delete the ingresso
        restIngressoMockMvc
            .perform(delete(ENTITY_API_URL_ID, ingresso.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Ingresso> ingressoList = ingressoRepository.findAll();
        assertThat(ingressoList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
