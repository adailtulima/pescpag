/**
 * View Models used by Spring MVC REST controllers.
 */
package br.ufpa.castanhal.web.rest.vm;
