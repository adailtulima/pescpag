package br.ufpa.castanhal.domain;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Ingresso.
 */
@Entity
@Table(name = "ingresso")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Ingresso implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "dia")
    private LocalDate dia;

    @Column(name = "valor")
    private Double valor;

    @Column(name = "hora_saida")
    private ZonedDateTime horaSaida;

    @Column(name = "hora_entrada")
    private ZonedDateTime horaEntrada;

    @ManyToOne
    private Pessoa pessoa;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Ingresso id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDia() {
        return this.dia;
    }

    public Ingresso dia(LocalDate dia) {
        this.setDia(dia);
        return this;
    }

    public void setDia(LocalDate dia) {
        this.dia = dia;
    }

    public Double getValor() {
        return this.valor;
    }

    public Ingresso valor(Double valor) {
        this.setValor(valor);
        return this;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public ZonedDateTime getHoraSaida() {
        return this.horaSaida;
    }

    public Ingresso horaSaida(ZonedDateTime horaSaida) {
        this.setHoraSaida(horaSaida);
        return this;
    }

    public void setHoraSaida(ZonedDateTime horaSaida) {
        this.horaSaida = horaSaida;
    }

    public ZonedDateTime getHoraEntrada() {
        return this.horaEntrada;
    }

    public Ingresso horaEntrada(ZonedDateTime horaEntrada) {
        this.setHoraEntrada(horaEntrada);
        return this;
    }

    public void setHoraEntrada(ZonedDateTime horaEntrada) {
        this.horaEntrada = horaEntrada;
    }

    public Pessoa getPessoa() {
        return this.pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    public Ingresso pessoa(Pessoa pessoa) {
        this.setPessoa(pessoa);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Ingresso)) {
            return false;
        }
        return id != null && id.equals(((Ingresso) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Ingresso{" +
            "id=" + getId() +
            ", dia='" + getDia() + "'" +
            ", valor=" + getValor() +
            ", horaSaida='" + getHoraSaida() + "'" +
            ", horaEntrada='" + getHoraEntrada() + "'" +
            "}";
    }
}
