package br.ufpa.castanhal.repository;

import br.ufpa.castanhal.domain.Ingresso;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Ingresso entity.
 */
@Repository
public interface IngressoRepository extends JpaRepository<Ingresso, Long> {
    default Optional<Ingresso> findOneWithEagerRelationships(Long id) {
        return this.findOneWithToOneRelationships(id);
    }

    default List<Ingresso> findAllWithEagerRelationships() {
        return this.findAllWithToOneRelationships();
    }

    default Page<Ingresso> findAllWithEagerRelationships(Pageable pageable) {
        return this.findAllWithToOneRelationships(pageable);
    }

    @Query(
        value = "select distinct ingresso from Ingresso ingresso left join fetch ingresso.pessoa",
        countQuery = "select count(distinct ingresso) from Ingresso ingresso"
    )
    Page<Ingresso> findAllWithToOneRelationships(Pageable pageable);

    @Query("select distinct ingresso from Ingresso ingresso left join fetch ingresso.pessoa")
    List<Ingresso> findAllWithToOneRelationships();

    @Query("select ingresso from Ingresso ingresso left join fetch ingresso.pessoa where ingresso.id =:id")
    Optional<Ingresso> findOneWithToOneRelationships(@Param("id") Long id);
}
