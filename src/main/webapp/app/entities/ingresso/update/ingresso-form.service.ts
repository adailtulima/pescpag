import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import dayjs from 'dayjs/esm';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { IIngresso, NewIngresso } from '../ingresso.model';

/**
 * A partial Type with required key is used as form input.
 */
type PartialWithRequiredKeyOf<T extends { id: unknown }> = Partial<Omit<T, 'id'>> & { id: T['id'] };

/**
 * Type for createFormGroup and resetForm argument.
 * It accepts IIngresso for edit and NewIngressoFormGroupInput for create.
 */
type IngressoFormGroupInput = IIngresso | PartialWithRequiredKeyOf<NewIngresso>;

/**
 * Type that converts some properties for forms.
 */
type FormValueOf<T extends IIngresso | NewIngresso> = Omit<T, 'horaSaida' | 'horaEntrada'> & {
  horaSaida?: string | null;
  horaEntrada?: string | null;
};

type IngressoFormRawValue = FormValueOf<IIngresso>;

type NewIngressoFormRawValue = FormValueOf<NewIngresso>;

type IngressoFormDefaults = Pick<NewIngresso, 'id' | 'horaSaida' | 'horaEntrada'>;

type IngressoFormGroupContent = {
  id: FormControl<IngressoFormRawValue['id'] | NewIngresso['id']>;
  dia: FormControl<IngressoFormRawValue['dia']>;
  valor: FormControl<IngressoFormRawValue['valor']>;
  horaSaida: FormControl<IngressoFormRawValue['horaSaida']>;
  horaEntrada: FormControl<IngressoFormRawValue['horaEntrada']>;
  pessoa: FormControl<IngressoFormRawValue['pessoa']>;
};

export type IngressoFormGroup = FormGroup<IngressoFormGroupContent>;

@Injectable({ providedIn: 'root' })
export class IngressoFormService {
  createIngressoFormGroup(ingresso: IngressoFormGroupInput = { id: null }): IngressoFormGroup {
    const ingressoRawValue = this.convertIngressoToIngressoRawValue({
      ...this.getFormDefaults(),
      ...ingresso,
    });
    return new FormGroup<IngressoFormGroupContent>({
      id: new FormControl(
        { value: ingressoRawValue.id, disabled: true },
        {
          nonNullable: true,
          validators: [Validators.required],
        }
      ),
      dia: new FormControl(ingressoRawValue.dia),
      valor: new FormControl(ingressoRawValue.valor),
      horaSaida: new FormControl(ingressoRawValue.horaSaida),
      horaEntrada: new FormControl(ingressoRawValue.horaEntrada),
      pessoa: new FormControl(ingressoRawValue.pessoa),
    });
  }

  getIngresso(form: IngressoFormGroup): IIngresso | NewIngresso {
    return this.convertIngressoRawValueToIngresso(form.getRawValue() as IngressoFormRawValue | NewIngressoFormRawValue);
  }

  resetForm(form: IngressoFormGroup, ingresso: IngressoFormGroupInput): void {
    const ingressoRawValue = this.convertIngressoToIngressoRawValue({ ...this.getFormDefaults(), ...ingresso });
    form.reset(
      {
        ...ingressoRawValue,
        id: { value: ingressoRawValue.id, disabled: true },
      } as any /* cast to workaround https://github.com/angular/angular/issues/46458 */
    );
  }

  private getFormDefaults(): IngressoFormDefaults {
    const currentTime = dayjs();

    return {
      id: null,
      horaSaida: currentTime,
      horaEntrada: currentTime,
    };
  }

  private convertIngressoRawValueToIngresso(rawIngresso: IngressoFormRawValue | NewIngressoFormRawValue): IIngresso | NewIngresso {
    return {
      ...rawIngresso,
      horaSaida: dayjs(rawIngresso.horaSaida, DATE_TIME_FORMAT),
      horaEntrada: dayjs(rawIngresso.horaEntrada, DATE_TIME_FORMAT),
    };
  }

  private convertIngressoToIngressoRawValue(
    ingresso: IIngresso | (Partial<NewIngresso> & IngressoFormDefaults)
  ): IngressoFormRawValue | PartialWithRequiredKeyOf<NewIngressoFormRawValue> {
    return {
      ...ingresso,
      horaSaida: ingresso.horaSaida ? ingresso.horaSaida.format(DATE_TIME_FORMAT) : undefined,
      horaEntrada: ingresso.horaEntrada ? ingresso.horaEntrada.format(DATE_TIME_FORMAT) : undefined,
    };
  }
}
