import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IngressoFormService, IngressoFormGroup } from './ingresso-form.service';
import { IIngresso } from '../ingresso.model';
import { IngressoService } from '../service/ingresso.service';
import { IPessoa } from 'app/entities/pessoa/pessoa.model';
import { PessoaService } from 'app/entities/pessoa/service/pessoa.service';

@Component({
  selector: 'jhi-ingresso-update',
  templateUrl: './ingresso-update.component.html',
})
export class IngressoUpdateComponent implements OnInit {
  isSaving = false;
  ingresso: IIngresso | null = null;

  pessoasSharedCollection: IPessoa[] = [];

  editForm: IngressoFormGroup = this.ingressoFormService.createIngressoFormGroup();

  constructor(
    protected ingressoService: IngressoService,
    protected ingressoFormService: IngressoFormService,
    protected pessoaService: PessoaService,
    protected activatedRoute: ActivatedRoute
  ) {}

  comparePessoa = (o1: IPessoa | null, o2: IPessoa | null): boolean => this.pessoaService.comparePessoa(o1, o2);

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ ingresso }) => {
      this.ingresso = ingresso;
      if (ingresso) {
        this.updateForm(ingresso);
      }

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const ingresso = this.ingressoFormService.getIngresso(this.editForm);
    if (ingresso.id !== null) {
      this.subscribeToSaveResponse(this.ingressoService.update(ingresso));
    } else {
      this.subscribeToSaveResponse(this.ingressoService.create(ingresso));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IIngresso>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(ingresso: IIngresso): void {
    this.ingresso = ingresso;
    this.ingressoFormService.resetForm(this.editForm, ingresso);

    this.pessoasSharedCollection = this.pessoaService.addPessoaToCollectionIfMissing<IPessoa>(
      this.pessoasSharedCollection,
      ingresso.pessoa
    );
  }

  protected loadRelationshipsOptions(): void {
    this.pessoaService
      .query()
      .pipe(map((res: HttpResponse<IPessoa[]>) => res.body ?? []))
      .pipe(map((pessoas: IPessoa[]) => this.pessoaService.addPessoaToCollectionIfMissing<IPessoa>(pessoas, this.ingresso?.pessoa)))
      .subscribe((pessoas: IPessoa[]) => (this.pessoasSharedCollection = pessoas));
  }
}
