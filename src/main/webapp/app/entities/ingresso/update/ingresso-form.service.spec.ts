import { TestBed } from '@angular/core/testing';

import { sampleWithRequiredData, sampleWithNewData } from '../ingresso.test-samples';

import { IngressoFormService } from './ingresso-form.service';

describe('Ingresso Form Service', () => {
  let service: IngressoFormService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(IngressoFormService);
  });

  describe('Service methods', () => {
    describe('createIngressoFormGroup', () => {
      it('should create a new form with FormControl', () => {
        const formGroup = service.createIngressoFormGroup();

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            dia: expect.any(Object),
            valor: expect.any(Object),
            horaSaida: expect.any(Object),
            horaEntrada: expect.any(Object),
            pessoa: expect.any(Object),
          })
        );
      });

      it('passing IIngresso should create a new form with FormGroup', () => {
        const formGroup = service.createIngressoFormGroup(sampleWithRequiredData);

        expect(formGroup.controls).toEqual(
          expect.objectContaining({
            id: expect.any(Object),
            dia: expect.any(Object),
            valor: expect.any(Object),
            horaSaida: expect.any(Object),
            horaEntrada: expect.any(Object),
            pessoa: expect.any(Object),
          })
        );
      });
    });

    describe('getIngresso', () => {
      it('should return NewIngresso for default Ingresso initial value', () => {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        const formGroup = service.createIngressoFormGroup(sampleWithNewData);

        const ingresso = service.getIngresso(formGroup) as any;

        expect(ingresso).toMatchObject(sampleWithNewData);
      });

      it('should return NewIngresso for empty Ingresso initial value', () => {
        const formGroup = service.createIngressoFormGroup();

        const ingresso = service.getIngresso(formGroup) as any;

        expect(ingresso).toMatchObject({});
      });

      it('should return IIngresso', () => {
        const formGroup = service.createIngressoFormGroup(sampleWithRequiredData);

        const ingresso = service.getIngresso(formGroup) as any;

        expect(ingresso).toMatchObject(sampleWithRequiredData);
      });
    });

    describe('resetForm', () => {
      it('passing IIngresso should not enable id FormControl', () => {
        const formGroup = service.createIngressoFormGroup();
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, sampleWithRequiredData);

        expect(formGroup.controls.id.disabled).toBe(true);
      });

      it('passing NewIngresso should disable id FormControl', () => {
        const formGroup = service.createIngressoFormGroup(sampleWithRequiredData);
        expect(formGroup.controls.id.disabled).toBe(true);

        service.resetForm(formGroup, { id: null });

        expect(formGroup.controls.id.disabled).toBe(true);
      });
    });
  });
});
