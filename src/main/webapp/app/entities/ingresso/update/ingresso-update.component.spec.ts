import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { IngressoFormService } from './ingresso-form.service';
import { IngressoService } from '../service/ingresso.service';
import { IIngresso } from '../ingresso.model';
import { IPessoa } from 'app/entities/pessoa/pessoa.model';
import { PessoaService } from 'app/entities/pessoa/service/pessoa.service';

import { IngressoUpdateComponent } from './ingresso-update.component';

describe('Ingresso Management Update Component', () => {
  let comp: IngressoUpdateComponent;
  let fixture: ComponentFixture<IngressoUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let ingressoFormService: IngressoFormService;
  let ingressoService: IngressoService;
  let pessoaService: PessoaService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [IngressoUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(IngressoUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(IngressoUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    ingressoFormService = TestBed.inject(IngressoFormService);
    ingressoService = TestBed.inject(IngressoService);
    pessoaService = TestBed.inject(PessoaService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Pessoa query and add missing value', () => {
      const ingresso: IIngresso = { id: 456 };
      const pessoa: IPessoa = { id: 77137 };
      ingresso.pessoa = pessoa;

      const pessoaCollection: IPessoa[] = [{ id: 3964 }];
      jest.spyOn(pessoaService, 'query').mockReturnValue(of(new HttpResponse({ body: pessoaCollection })));
      const additionalPessoas = [pessoa];
      const expectedCollection: IPessoa[] = [...additionalPessoas, ...pessoaCollection];
      jest.spyOn(pessoaService, 'addPessoaToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ ingresso });
      comp.ngOnInit();

      expect(pessoaService.query).toHaveBeenCalled();
      expect(pessoaService.addPessoaToCollectionIfMissing).toHaveBeenCalledWith(
        pessoaCollection,
        ...additionalPessoas.map(expect.objectContaining)
      );
      expect(comp.pessoasSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const ingresso: IIngresso = { id: 456 };
      const pessoa: IPessoa = { id: 90648 };
      ingresso.pessoa = pessoa;

      activatedRoute.data = of({ ingresso });
      comp.ngOnInit();

      expect(comp.pessoasSharedCollection).toContain(pessoa);
      expect(comp.ingresso).toEqual(ingresso);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IIngresso>>();
      const ingresso = { id: 123 };
      jest.spyOn(ingressoFormService, 'getIngresso').mockReturnValue(ingresso);
      jest.spyOn(ingressoService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ ingresso });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: ingresso }));
      saveSubject.complete();

      // THEN
      expect(ingressoFormService.getIngresso).toHaveBeenCalled();
      expect(comp.previousState).toHaveBeenCalled();
      expect(ingressoService.update).toHaveBeenCalledWith(expect.objectContaining(ingresso));
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IIngresso>>();
      const ingresso = { id: 123 };
      jest.spyOn(ingressoFormService, 'getIngresso').mockReturnValue({ id: null });
      jest.spyOn(ingressoService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ ingresso: null });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: ingresso }));
      saveSubject.complete();

      // THEN
      expect(ingressoFormService.getIngresso).toHaveBeenCalled();
      expect(ingressoService.create).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<IIngresso>>();
      const ingresso = { id: 123 };
      jest.spyOn(ingressoService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ ingresso });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(ingressoService.update).toHaveBeenCalled();
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Compare relationships', () => {
    describe('comparePessoa', () => {
      it('Should forward to pessoaService', () => {
        const entity = { id: 123 };
        const entity2 = { id: 456 };
        jest.spyOn(pessoaService, 'comparePessoa');
        comp.comparePessoa(entity, entity2);
        expect(pessoaService.comparePessoa).toHaveBeenCalledWith(entity, entity2);
      });
    });
  });
});
