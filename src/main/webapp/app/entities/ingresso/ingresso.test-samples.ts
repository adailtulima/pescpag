import dayjs from 'dayjs/esm';

import { IIngresso, NewIngresso } from './ingresso.model';

export const sampleWithRequiredData: IIngresso = {
  id: 13734,
};

export const sampleWithPartialData: IIngresso = {
  id: 42632,
  horaSaida: dayjs('2022-09-12T12:12'),
};

export const sampleWithFullData: IIngresso = {
  id: 34707,
  dia: dayjs('2022-09-12'),
  valor: 80333,
  horaSaida: dayjs('2022-09-12T03:28'),
  horaEntrada: dayjs('2022-09-12T11:01'),
};

export const sampleWithNewData: NewIngresso = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
