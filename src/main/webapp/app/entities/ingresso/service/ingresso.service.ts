import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IIngresso, NewIngresso } from '../ingresso.model';

export type PartialUpdateIngresso = Partial<IIngresso> & Pick<IIngresso, 'id'>;

type RestOf<T extends IIngresso | NewIngresso> = Omit<T, 'dia' | 'horaSaida' | 'horaEntrada'> & {
  dia?: string | null;
  horaSaida?: string | null;
  horaEntrada?: string | null;
};

export type RestIngresso = RestOf<IIngresso>;

export type NewRestIngresso = RestOf<NewIngresso>;

export type PartialUpdateRestIngresso = RestOf<PartialUpdateIngresso>;

export type EntityResponseType = HttpResponse<IIngresso>;
export type EntityArrayResponseType = HttpResponse<IIngresso[]>;

@Injectable({ providedIn: 'root' })
export class IngressoService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/ingressos');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(ingresso: NewIngresso): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(ingresso);
    return this.http
      .post<RestIngresso>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  update(ingresso: IIngresso): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(ingresso);
    return this.http
      .put<RestIngresso>(`${this.resourceUrl}/${this.getIngressoIdentifier(ingresso)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  partialUpdate(ingresso: PartialUpdateIngresso): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(ingresso);
    return this.http
      .patch<RestIngresso>(`${this.resourceUrl}/${this.getIngressoIdentifier(ingresso)}`, copy, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<RestIngresso>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map(res => this.convertResponseFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<RestIngresso[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map(res => this.convertResponseArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  getIngressoIdentifier(ingresso: Pick<IIngresso, 'id'>): number {
    return ingresso.id;
  }

  compareIngresso(o1: Pick<IIngresso, 'id'> | null, o2: Pick<IIngresso, 'id'> | null): boolean {
    return o1 && o2 ? this.getIngressoIdentifier(o1) === this.getIngressoIdentifier(o2) : o1 === o2;
  }

  addIngressoToCollectionIfMissing<Type extends Pick<IIngresso, 'id'>>(
    ingressoCollection: Type[],
    ...ingressosToCheck: (Type | null | undefined)[]
  ): Type[] {
    const ingressos: Type[] = ingressosToCheck.filter(isPresent);
    if (ingressos.length > 0) {
      const ingressoCollectionIdentifiers = ingressoCollection.map(ingressoItem => this.getIngressoIdentifier(ingressoItem)!);
      const ingressosToAdd = ingressos.filter(ingressoItem => {
        const ingressoIdentifier = this.getIngressoIdentifier(ingressoItem);
        if (ingressoCollectionIdentifiers.includes(ingressoIdentifier)) {
          return false;
        }
        ingressoCollectionIdentifiers.push(ingressoIdentifier);
        return true;
      });
      return [...ingressosToAdd, ...ingressoCollection];
    }
    return ingressoCollection;
  }

  protected convertDateFromClient<T extends IIngresso | NewIngresso | PartialUpdateIngresso>(ingresso: T): RestOf<T> {
    return {
      ...ingresso,
      dia: ingresso.dia?.format(DATE_FORMAT) ?? null,
      horaSaida: ingresso.horaSaida?.toJSON() ?? null,
      horaEntrada: ingresso.horaEntrada?.toJSON() ?? null,
    };
  }

  protected convertDateFromServer(restIngresso: RestIngresso): IIngresso {
    return {
      ...restIngresso,
      dia: restIngresso.dia ? dayjs(restIngresso.dia) : undefined,
      horaSaida: restIngresso.horaSaida ? dayjs(restIngresso.horaSaida) : undefined,
      horaEntrada: restIngresso.horaEntrada ? dayjs(restIngresso.horaEntrada) : undefined,
    };
  }

  protected convertResponseFromServer(res: HttpResponse<RestIngresso>): HttpResponse<IIngresso> {
    return res.clone({
      body: res.body ? this.convertDateFromServer(res.body) : null,
    });
  }

  protected convertResponseArrayFromServer(res: HttpResponse<RestIngresso[]>): HttpResponse<IIngresso[]> {
    return res.clone({
      body: res.body ? res.body.map(item => this.convertDateFromServer(item)) : null,
    });
  }
}
