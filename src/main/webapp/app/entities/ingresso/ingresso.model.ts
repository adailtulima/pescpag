import dayjs from 'dayjs/esm';
import { IPessoa } from 'app/entities/pessoa/pessoa.model';

export interface IIngresso {
  id: number;
  dia?: dayjs.Dayjs | null;
  valor?: number | null;
  horaSaida?: dayjs.Dayjs | null;
  horaEntrada?: dayjs.Dayjs | null;
  pessoa?: Pick<IPessoa, 'id' | 'nome'> | null;
}

export type NewIngresso = Omit<IIngresso, 'id'> & { id: null };
