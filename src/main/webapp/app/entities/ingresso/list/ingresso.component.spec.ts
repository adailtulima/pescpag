import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { IngressoService } from '../service/ingresso.service';

import { IngressoComponent } from './ingresso.component';

describe('Ingresso Management Component', () => {
  let comp: IngressoComponent;
  let fixture: ComponentFixture<IngressoComponent>;
  let service: IngressoService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes([{ path: 'ingresso', component: IngressoComponent }]), HttpClientTestingModule],
      declarations: [IngressoComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            data: of({
              defaultSort: 'id,asc',
            }),
            queryParamMap: of(
              jest.requireActual('@angular/router').convertToParamMap({
                page: '1',
                size: '1',
                sort: 'id,desc',
              })
            ),
            snapshot: { queryParams: {} },
          },
        },
      ],
    })
      .overrideTemplate(IngressoComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(IngressoComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(IngressoService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.ingressos?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });

  describe('trackId', () => {
    it('Should forward to ingressoService', () => {
      const entity = { id: 123 };
      jest.spyOn(service, 'getIngressoIdentifier');
      const id = comp.trackId(0, entity);
      expect(service.getIngressoIdentifier).toHaveBeenCalledWith(entity);
      expect(id).toBe(entity.id);
    });
  });
});
