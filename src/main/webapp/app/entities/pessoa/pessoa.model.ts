export interface IPessoa {
  id: number;
  nome?: string | null;
  cpf?: string | null;
  telefone?: string | null;
  carro?: string | null;
  idade?: number | null;
}

export type NewPessoa = Omit<IPessoa, 'id'> & { id: null };
