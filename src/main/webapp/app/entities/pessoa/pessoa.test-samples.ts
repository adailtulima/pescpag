import { IPessoa, NewPessoa } from './pessoa.model';

export const sampleWithRequiredData: IPessoa = {
  id: 82183,
};

export const sampleWithPartialData: IPessoa = {
  id: 91510,
  telefone: 'Licenciado hub Business-focused',
  carro: 'supply-chains Pequeno',
};

export const sampleWithFullData: IPessoa = {
  id: 70057,
  nome: 'incremental',
  cpf: 'mobile',
  telefone: 'Research',
  carro: 'Turismo Diverse',
  idade: 88083,
};

export const sampleWithNewData: NewPessoa = {
  id: null,
};

Object.freeze(sampleWithNewData);
Object.freeze(sampleWithRequiredData);
Object.freeze(sampleWithPartialData);
Object.freeze(sampleWithFullData);
